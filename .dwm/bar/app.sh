#!/bin/sh

# -----------------
# Dependencies: 
# -----------------
# all systems :
# - xorg-xsetroot
# - alsa-utils
# - pulseaudio
# -----------------
# laptop specific :
# - light

# -----------------
# Color palette :
# -----------------
# 264653
# 2a9d8f
# e9c46a
# f4a261
# e76f51

export color_0="264653" # dark blue
export color_1="bbbbbb" # light grey
export color_2="597986" # middle blue
export color_3="222222" # dark grey

# Store the directory the script is running from
LOC=$(readlink -f "$0")
DIR=$(dirname "$LOC")

prefix="mod_"
space="   "

# Modules to handle
modules=""
modules="$modules network" 
modules="$modules volume" 
[ -e "/usr/bin/light" ] && modules="$modules backlight" || modules="$modules"
[ -d "/sys/class/power_supply/BAT1" ] && modules="$modules battery" || modules="$modules"
modules="$modules date" 
modules="$modules time" 

# import modules
for module in $modules
  do . "$DIR/modules/$prefix$module.sh"
done

# Update dwm status bar every x time
while true
do
  # Append results of each func one by one to the upperbar string
  #upperbar="$upperbar${__DWM_BAR_WEATHER__}"
  #upperbar="$upperbar${__DWM_BAR_NETWORKMANAGER__}"
  upperbar=""
  for module in $modules
    do upperbar="$upperbar$space$($prefix$module)"
  done
  
  # Append results of each func one by one to the lowerbar string
  lowerbar=""

  xsetroot -name "$upperbar"
  
  # Uncomment the line below to enable the lowerbar 
  # xsetroot -name "$upperbar;$lowerbar"
  sleep .1
done
