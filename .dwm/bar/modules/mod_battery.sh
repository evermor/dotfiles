#!/bin/sh
mod_battery () {
  
  ALERT_CAP=20

  # get datas
  capacity=$(cat /sys/class/power_supply/BAT1/capacity)
  status=$(cat /sys/class/power_supply/BAT1/status)

  printf "^c#$color_1^"
  
  if [ "$status" = "Charging" ]; then
    printf " %2d" "$capacity"

  elif [ "$capacity" -lt "$ALERT_CAP" -a `expr $(date "+%S") % 2` -eq 0 ]; then
      printf "  %2d" "$capacity"

  else
    case $((
      ($capacity > 95)  ? 1 :
      ($capacity > 90)  ? 2 :
      ($capacity > 80)  ? 3 :
      ($capacity > 70)  ? 4 :
      ($capacity > 60)  ? 5 :
      ($capacity > 50)  ? 6 :
      ($capacity > 40)  ? 7 :
      ($capacity > 30)  ? 8 :
      ($capacity > 20)  ? 9 :
      ($capacity > 10)  ? 10 : 0)) in
      (1)   printf " %2d" "$capacity";;
      (2)   printf " %2d" "$capacity";;
      (3)   printf " %2d" "$capacity";;
      (4)   printf " %2d" "$capacity";;
      (5)   printf " %2d" "$capacity";;
      (6)   printf " %2d" "$capacity";;
      (7)   printf " %2d" "$capacity";;
      (8)   printf " %2d" "$capacity";;
      (9)   printf " %2d" "$capacity";;
      (10)  printf " %2d" "$capacity";;
      (0)   printf " %2d" "$capacity";;
    esac
  fi
}

mod_battery

