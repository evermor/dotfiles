#!/bin/sh
# Dependencies: alsa-utils
mod_volume () {
  len=10
  in1=$(amixer sget Master |tail -n1 |sed -r "s/.*\[(.*)\]/\1/")
  in2=$(amixer get Master |tail -n1 |sed -r "s/.*\[(.*)%\].*/\1/")
  lvl=`expr $in2 \* $len / 100` ; res=`expr $len - $lvl`
  printf "^c#$color_1^" ; [ $in1 = "off" ] && printf "ﱝ " || printf "奔 " &&
  printf "%0${lvl}s" |sed -r "s/ //g" && printf "^c#$color_0^" &&
  printf "%0${res}s" |sed -r "s/ //g"
}

mod_volume
