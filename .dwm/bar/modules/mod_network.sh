#!/bin/sh
# Dependencies: nmcli
mod_network () {
  inp=$(nmcli -f ACTIVE,SSID,SIGNAL dev wifi list | awk '$1=="oui" {printf "%s %3d", $2, $3}')
  [$inp = ""] && printf "睊" || printf "直 %s" "$inp"
}

mod_network
