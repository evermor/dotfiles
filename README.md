# Evermor's Dotfiles & suckless environment

HOW TO INSTALL DOTFILES ON A NEW SYSTEM

1 - git clone --bare https://gitlab.com/evermor/dotfiles.git $HOME/.dotfiles.git
2 - echo alias dot='/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME' >> ~/.bashrc
3 - dot checkout
4 - [Remove or back up conflicting files]
5 - dot checkout [again]
6 - dot config --local status.showUntrackedFiles all
7 - echo [include] path = .dotconfig >> ~/.gitconfig

USAGE

dot status
dot add example.file
dot commit -m "Add example.file"
dot push origin master

SYNC

- vscode via github account

DOC

- intl us european keybord layout
    https://blog.getreu.net/20201002-international-EurKEY-US-keyboard-layout-Debian/

PACKAGES

## package manager
- snap

## suckless
- suckless-tools
- dwm
- st
- vim
- sxiv
- slock
- yt-dlp

## important
- isenkram-cli (commande isenkram-autoinstall-firmware pour détecter et installer firmwares manquants)
- pass (+passmenu)
- gpg
- git
- sudo
- man-db
- bash-completion
- locales
- console-setup
- build-essential
- libx11-dev
- libxrandr-dev
- libxft-dev
- libxinerama-dev
- xserver-xorg
- xserver-xorg-video-fbdev
- xinit
- pciutils
- xinput
- xfonts-100dpi
- xfonts-75dpi
- xfonts-scalable
- x11-xserver-utils
- unifont
- alsa-utils
- pulseaudio
- pulseaudio-module-bluetooth
- bluez
- network-manager
- nvidia-detect

## apps
- draw.io
- piskel
- gimp
- wine

## utils
- inxi
- ytfzf
- dict
- jq
- wiggle
- wget
- inxi
- traceroute
- ufw
- fdisk
- keepass2
- zip
- neofetch
- time
- tree
- htop
- pywal
- wmctrl
- feh
- compton
- grub-customizer

## laptop specific
- tlp
- light (from .deb)

## intel specific
- firmware-misc-nonfree
- firmware-iwlwifi
- xserver-xorg-video-intel (system crash on msi gf66 katana ??)
- intel-microcode (from sources)
