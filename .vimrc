" use Vim mode instead of pure Vi, it must be the first instruction
set nocompatible

" display settings
syntax enable
set background=dark
set encoding=utf-8              " encoding used for displaying file
set ruler                       " show the cursor position all the time
set showmatch                   " highlight matching braces
set showmode                    " show insert/replace/visual mode
set number                      " show line numbers
set nowrap
set linebreak
set laststatus=2
set showtabline=2
"set tabline=%t
set scrolloff=1
set wildmenu
set tabpagemax=10
set noautochdir

" write settings
set confirm                     " confirm :q in case of unsaved changes
set fileencoding=utf-8          " encoding used when saving file
set hidden
set nobackup
set nowritebackup
set cmdheight=1
set updatetime=300
set shortmess+=c

" edit settings
set backspace=indent,eol,start  " backspacing over everything in insert mode
set nojoinspaces                " no extra space after '.' when joining lines
set shiftwidth=4                " set indentation depth to 8 columns
set shiftround                  " when shifting lines, round the indentation to the nearest multiple of “shiftwidth”
set softtabstop=2               " backspacing over 8 spaces like over tabs
set autoindent                  " autoindent
set smartindent                 " smartindent
set expandtab
set smarttab                    " insert “tabstop” number of spaces when the “tab” key is pressed
set tabstop=2

" search settings
set hlsearch                    " highlight search results
set ignorecase                  " do case insensitive search...
set incsearch                   " do incremental search
set smartcase                   " ...unless capital letters are used

" navigation
:set path+=**

" modes
set timeoutlen=1000 ttimeoutlen=0

" file type specific settings
filetype on                     " enable file type detection
filetype plugin on              " load the plugins for specific file types
filetype indent on              " automatically indent code

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" custom commands
"---------------------------------------------------------------------
" key codes from sed -n l (in terminal)
execute "set <A-L>=\033[1;3D"
execute "set <A-R>=\033[1;3C"
"execute "set <A-1>=\033&"
"execute "set <A-2>=\303\251"
"execute "set <A-3>=\033\""
"execute "set <A-4>=\033&"
"execute "set <A-5>=\033&"
"execute "set <A-6>=\033&"
"execute "set <A-7>=\033&"
"execute "set <A-8>=\033&"
"execute "set <A-9>=\033&"
nnoremap  <C-i>     gg=G        " auto-indent the entire file with current options 
nnoremap  <A-L>     :bp<CR>     " prev buffer
nnoremap  <A-R>     :bn<CR>     " next buffer
nnoremap  <C-w>     :bd<CR>     " close active buffer
"nnoremap  <A-1>     :b1<CR>
"nnoremap  <A-2>     :b2<CR>
"nnoremap  <A-3>     :b3<CR>
"nnoremap  <A-4>     :b4<CR>
"nnoremap  <A-5>     :b5<CR>
"nnoremap  <A-6>     :b6<CR>
"nnoremap  <A-7>     :b7<CR>
"nnoremap  <A-8>     :b8<CR>
"nnoremap  <A-9>     :b9<CR>
nnoremap  <C-o>     :Files<CR>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" plugins (managed by ~/.vim/autoload/plug.vim from junegunn/vim-plug)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" :PlugInstall                : install plugins
" :PlugUpdate                 : update plugins
" :PlugClean                  : remove plugins
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
call plug#begin()
" coc :
Plug 'neoclide/coc.nvim', {'branch': 'release'}
" apparence :
Plug 'dylanaraps/wal.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" color syntax :
Plug 'nelsyeung/twig.vim'
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
" productivity :
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'mattn/emmet-vim'
Plug 'tpope/vim-surround'
call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" coc
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:coc_config_home = "~/.config/coc"
let g:coc_global_extensions = [
      \ 'coc-tsserver',
      \ 'coc-prettier',
      \ 'coc-pairs',
      \ 'coc-json',
      \ 'coc-css',
      \ ]

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-airline
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"let g:airline_statusline_ontop=1
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled=1
let g:airline#extensions#tabline#formatter='unique_tail_improved'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" emmet-vim
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:user_emmet_leader_key='<C-l>'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" VimEnter events - do things at startup
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd vimEnter * ++nested colorscheme wal
