# ~/.bash_aliases

# fast commands :
alias c="clear"
alias l="ls -lAXFh --group-directories-first --color"
alias a="cd .."
alias h="history"
alias hg="history | grep"

# git dotfiles command
alias dot="/usr/bin/git --git-dir=$HOME/.dotfiles.git/ --work-tree=$HOME"

