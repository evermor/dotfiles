#!/usr/bin/python3
#author : Julien Jacquart

import importlib
import importlib.util
import argparse
from pprint import pprint

parser = argparse.ArgumentParser(description="Minimalist Python Documentation Interface.")

# positional arguments
parser.add_argument("module", help="the name of an existing module")

# optional arguments
parser.add_argument("-f", "--function", help="the name of an existing function")

# get args
args = parser.parse_args(); 

if importlib.util.find_spec(args.module) is not None:
    if args.function:
        help(f"{args.module}.{args.function}")
    else:
        mod = importlib.import_module(args.module)
        pprint(dir(mod))
else:
    print("Module not found")
 
